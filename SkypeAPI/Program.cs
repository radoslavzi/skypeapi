﻿using System;
using System.Configuration;
using System.Threading;
using SKYPE4COMLib;
using System.Collections.Generic;

namespace SkypeAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            SkypeWrapper wrapper = new SkypeWrapper();
            wrapper.ProcessReceivedMessages();
        }
    }
}
