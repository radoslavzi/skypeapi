﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;

namespace SkypeAPI
{
    public class SkypeWrapper : ICommunicationWrapper
    {
        private Skype _skype;
        private string _defaultMessage;
        private IDictionary<string, bool> _chatUsers;
        private User _sender;

        public SkypeWrapper()
        {
            this._skype = new Skype();
            this._chatUsers = new Dictionary<string, bool>();
            this._defaultMessage = ConfigurationManager.AppSettings["SkypeAutoReplyMessage"];

            this.AttachProcess();
        }

        public SkypeWrapper(string defaultMessage)
            : this()
        {
            this._defaultMessage = defaultMessage;
        }

        public bool TryAnswerBasicQuestion(string question)
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[question]))
            {
                this.SendMessage(ConfigurationManager.AppSettings[question]);
                this._chatUsers[this._sender.FullName] = true;
                return true;
            }

            string[] basicQuestions = ConfigurationManager.AppSettings["SkypeBasicQuestions"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string item in basicQuestions)
            {
                if (question.Contains(item))
                {
                    this.SendMessage(ConfigurationManager.AppSettings[item]);
                    this._chatUsers[this._sender.FullName] = true;
                    return true;
                }
            }

            return false;
        }

        public void AttachProcess()
        {
            this._skype.Attach();
        }

        public void SendMessage(string message)
        {
            this._skype.SendMessage(this._sender.Handle, string.Format("{0}, {1}", this._sender.FullName, message));
            Console.WriteLine(string.Format("Message sent to {0}", this._sender.FullName));
        }

        public void ProcessReceivedMessages()
        {
            while (true)
            {
                foreach (IChatMessage msg in this._skype.MissedMessages)
                {
                    this._sender = msg.Sender;
                    this.UpdateChatUsers(this._sender.FullName);

                    if (!this._chatUsers[this._sender.FullName] && !this.TryAnswerBasicQuestion(msg.Body.ToLowerInvariant()) && this._skype.CurrentUserStatus != TUserStatus.cusOnline)
                    {
                        this.SendMessage(this._defaultMessage);
                    }
                }

                this._skype.MissedMessages.RemoveAll();
                this.ChangeStatus();
                Thread.Sleep(5000);
            }
        }

        public void ChangeStatus()
        {
            if (!this.IsUserLoggedOn())
            {
                this._skype.ChangeUserStatus(TUserStatus.cusAway);
            }
            else
            {
                this._skype.ChangeUserStatus(TUserStatus.cusOnline);
            }
        }

        private bool IsUserLoggedOn()
        {
            Process[] process = Process.GetProcessesByName("winlogon");
            if (process.Length == 0)
                return false;
            else
                return true;
        }

        private void UpdateChatUsers(string userName)
        {
            if (!this._chatUsers.ContainsKey(this._sender.FullName))
            {
                this._chatUsers.Add(this._sender.FullName, false);
            }
            else
            {
                this._chatUsers[this._sender.FullName] = false;
            }
        }
    }
}
