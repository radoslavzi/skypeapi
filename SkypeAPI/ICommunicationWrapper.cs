﻿using System;

namespace SkypeAPI
{
    public interface ICommunicationWrapper
    {
        void AttachProcess();
        bool TryAnswerBasicQuestion(string question);
        void ProcessReceivedMessages();
        void SendMessage(string message);
        void ChangeStatus();
    }
}
